import request from '@/utils/request'

/**
 * 获取 搜索联想数据
 * @param {SearchText} q
 * @returns
 */
export const getSearchSuggesttion = (q) => {
  return request({
    url: '/suggestion',
    method: 'get',
    params: { q }
  })
}

/**
 * 获取 用户搜索结果
 * @param {page, per_page, q} params
 * @returns
 */
export const getSearchReslutes = (params) => {
  return request({
    url: '/suggestion',
    method: 'get',
    params
  })
}
