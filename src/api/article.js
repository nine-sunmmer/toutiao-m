import request from '@/utils/request'

/**
 *获取 文章新闻推荐
 * @param {channel_id, timestamp} params
 * @returns
 */
export const getArticle = (params) => {
  return request({
    method: 'get',
    url: '/articles',
    params
  })
}

/**
 * 获取 文章详情
 * @param {id} id
 * @returns
 */
export const getArticleDetail = (id) => {
  return request({
    method: 'get',
    url: '/articles/' + id
  })
}

/**
 * 收藏文章
 * @param {文章id} target
 * @returns
 */
export const collectArticle = (target) => {
  return request({
    method: 'post',
    url: '/article/collections',
    data: { target }
  })
}

/**
 *取消收藏文章
 * @param {文章id} target
 * @returns
 */
export const cancelCollectArticle = (target) => {
  return request({
    method: 'delete',
    url: '/article/collections/' + target
  })
}

/**
 * 点赞文章
 * @param {文章id} target
 * @returns
 */
export const ArtLike = (target) => {
  return request({
    method: 'post',
    url: '/article/likings',
    data: { target }
  })
}

/**
 *取消 文章点赞
 * @param {文章id} target
 * @returns
 */
export const cancelArtLike = (target) => {
  return request({
    method: 'delete',
    url: '/article/likings/' + target
  })
}

/**
 * 点赞 评论
 * @param {target} params
 * @returns
 */
export const commentLike = (data) => {
  return request({
    method: 'post',
    url: '/comment/likings',
    data
  })
}

/**
 * 取消 点赞 评论
 * @param {target} target
 * @returns
 */
export const cannelCommentLike = (target) => {
  return request({
    method: 'delete',
    url: '/comment/likings/' + target
  })
}

/**
 * 回复 评论 或 评论文章
 * @param {target, context, art_id} data
 * @returns
 */
export const conmmentArt = (data) => {
  return request({
    method: 'post',
    url: '/comments',
    data
  })
}
