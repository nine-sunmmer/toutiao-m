import request from '@/utils/request'

/**
 * 获取 用户 频道列表
 * @returns
 */
export const getChannel = () => {
  return request({
    method: 'get',
    url: '/user/channels'
  })
}

/**
 * 获取所有 频道列表
 * @returns
 */
export const getAllChannel = () => {
  return request({
    method: 'get',
    url: '/channels'
  })
}

/**
 * 设置用户频道
 * @param {channels, id, seq} data
 * @returns
 */
export const addChannel = (channels) => {
  return request({
    method: 'patch',
    url: '/user/channels',
    data: {
      channels: [channels]
    }
  })
}

/**
 *删除 指定频道
 * @param {id} id
 * @returns
 */
export const delChannel = (id) => {
  return request({
    method: 'delete',
    url: '/user/channels/' + id
  })
}
