import request from '@/utils/request'
import store from '@/store/index'
/**
 *登录功能
 * @param {mobile, password} data
 * @returns
 */
export function login (data) {
  return request({
    method: 'POST',
    url: '/authorizations',
    data
  })
}

/**
 * 发送 验证码
 * @param {mobile} data
 * @returns
 */
export const mobile = (data) => {
  return request({
    method: 'GET',
    url: '/sms/codes/' + data
  })
}

/**
 * 获取用户信息
 * @returns
 */
export const getUserInfo = () => {
  return request({
    url: '/user',
    method: 'GET'
  })
}

/**
 *获取 用户 详细信息
 * @param {id} id
 * @returns
 */
export const getUserDetailInfo = (id) => {
  return request({
    url: '/user/profile',
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + store.state.user.userToken.token
    }
  })
}
/**
 * 关注用户
 * @param {id} id
 * @returns
 */
export const attentionUser = (id) => {
  return request({
    url: '/user/followings',
    method: 'post',
    data: { target: id }
  })
}

/**
 * 取消关注用户
 * @param {id} id
 * @returns
 */
export const delAttentionUser = (id) => {
  return request({
    url: '/user/followings/' + id,
    method: 'delete'
  })
}

/**
 * 编辑用户资料
 * @param {*} data
 * @returns
 */
export const editUserInfo = (data) => {
  return request({
    url: 'user/profile',
    method: 'patch',
    data
  })
}

/**
 * 获取用户资料
 * @param {*} data
 * @returns
 */
export const getUserInfoResource = () => {
  return request({
    url: 'user/profile',
    method: 'get'
  })
}

/**
 * 上传 头像, FormaData 格式
 * @param {*} data
 * @returns
 */
export const updateImg = (data) => {
  return request({
    url: '/user/photo',
    method: 'patch',
    data
  })
}
