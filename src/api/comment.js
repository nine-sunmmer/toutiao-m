import request from '@/utils/request'

/**
 * 获取 评论 或 回复
 * @param {type, source, offset, limit} params
 * @returns
 */
export const getComments = (params) => {
  return request({
    method: 'get',
    url: '/comments',
    params
  })
}
