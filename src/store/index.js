import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
const req = require.context('./modules', true, /\.js$/)
const obj = {}
req.keys().forEach(value => {
  const key = value.substring(value.lastIndexOf('/') + 1, value.indexOf('.js'))
  obj[key] = req(value).default
})

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: obj
})
