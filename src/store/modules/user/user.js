export default {
  namespaced: true,
  state: {
    userToken: {}
  },
  mutations: {
    // 1. 更新token
    updateToken (state, token) {
      state.userToken = token
    }
  },
  actions: {}
}
