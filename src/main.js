import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 导入 全局公共样式
import '@/style/index.less'

// 导入 vant 组件库
import Vant from 'vant'
import 'vant/lib/index.css'

// 导入flexible 文件
import 'lib-flexible'

// 导入 dayjs
import '@/utils/dayjs'

// 注册Vant
Vue.use(Vant)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
