import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
  // 登录
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/MyLogin.vue')
  },

  // 编辑资料
  {
    path: '/editinfo',
    name: 'editinfo',
    component: () => import('@/views/EditInfo/EditorIndex.vue')
  },

  // 主页
  {
    path: '/',
    redirect: '/home',
    component: () => import('@/views/main/MyMain.vue'),
    children: [
      // 1. 默认子路由
      {
        path: '/home',
        name: 'home',
        component: () => import('@/views/main/Home/MyHome.vue')
      },

      // 2. 问答
      {
        path: '/qa',
        name: 'qa',
        component: () => import('@/views/main/qa/QA.vue')
      },

      // 3. 视频
      {
        path: '/video',
        name: 'video',
        component: () => import('@/views/main/Video/MyVideo.vue')
      },

      // 4. 我的
      {
        path: '/my',
        name: 'my',
        component: () => import('@/views/main/My/MyInfo.vue')
      },

      {
        path: '/demo',
        component: () => import('@/views/demoV.vue')
      }
    ]
  },

  // 搜索
  {
    path: '/search',
    name: 'search',
    component: () => import('@/views/seacrh/SearchIndex.vue')
  },

  // 文章详情
  {
    path: '/article/:articleId',
    name: 'article',
    component: () => import('@/views/ArticleDetail/ArticleDetail.vue'),
    props: true
  }
]

const router = new VueRouter({
  routes
})

export default router
