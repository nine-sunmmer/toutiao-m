// 导入axios 模块
import store from '@/store'
import axios from 'axios'
const request = axios.create({
  baseURL: '/v1_0',
  timeout: 5000
})
export const http = axios.create({
  baseURL: 'http://www.liulongbin.top:3006'
})

// 1 设置请求拦截器
request.interceptors.request.use(
  (config) => {
    // 1.1 判断是否有token
    // console.log(config)
    if (store.state.user.userToken.token) {
      config.headers.Authorization = 'Bearer ' + store.state.user.userToken.token
    }
    return config
  },
  (err) => {
    // console.log(err)
    return Promise.reject(err)
  })

// 2. 设置了 响应拦截器
request.interceptors.response.use(
  (response) => {
    return response
  },
  (err) => {
    // console.log(err)
    return Promise.reject(err)
  })
export default request
