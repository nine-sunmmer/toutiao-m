import dayjs from 'dayjs'
import Vue from 'vue'

import relativeTime from 'dayjs/plugin/relativeTime'

// 更改语言配置
import 'dayjs/locale/zh-cn'
dayjs.locale('zh-cn')

// 插件 获取 相对时间
dayjs.extend(relativeTime)

// 注册全局过滤器
Vue.filter('relativeTime', value => {
  return dayjs().to(dayjs(value))
})
