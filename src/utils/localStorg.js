export function getItem (key) {
  let data = null
  try {
    data = JSON.parse(localStorage.getItem(key))
    return data
  } catch (err) {
    return data
  }
}

export const setItem = (key, value) => {
  if (typeof value === 'object') { localStorage.setItem(key, JSON.stringify(value)) } else { localStorage.setItem(key, value) }
}
