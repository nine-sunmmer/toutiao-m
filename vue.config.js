const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      // 如果请求地址以/api打头,就出触发代理机制
      // http://localhost:8888/api/sys/login ->  http://ihrm.itheima.net/api/sys/login
      '/v1_0': {
        target: 'http://geek.itheima.net' // 我们要代理的真实接口地址
      }
    }
  }
})
