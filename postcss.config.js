module.exports = {
  plugins: {
    // 兼容版本低版本, vuecli2 已经内置了 再次配置会引起冲突
    // autoprefixer: {
    //   browsers: ['Android >= 4.0', 'iOS >= 8']
    // },
    'postcss-pxtorem': {
      // rootValue : number || funciton
      // 函数 可以接受一个参数 会有每个编译后 css 文件信息
      rootValue ({ file }) {
        return file.includes('vant') ? 37.5 : 75
      },

      // 配置 所有属性都会 进行转换
      propList: ['*'],
      //  排除 不需要转换的css
      exclude: 'markdown'
    }
  }
}
